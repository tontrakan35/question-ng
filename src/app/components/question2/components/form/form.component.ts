import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  positions: {code: string, name: string}[] = [];

  constructor(
    public dialogRef: MatDialogRef<FormComponent>,
    @Inject(MAT_DIALOG_DATA) public obj: { form: FormGroup },
  ) { }

  get form() { return this.obj.form as FormGroup }

  ngOnInit(): void {
    const store = localStorage.getItem('positions');
    this.positions = store ? JSON.parse(store) : [];
  }

  callback() {
    this.dialogRef.close({...this.form.value})
  }

}
