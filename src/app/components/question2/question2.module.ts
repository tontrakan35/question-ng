import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { Question2Component } from './question2.component';
import { FormComponent } from './components/form/form.component';
import { DialogConfirmComponent } from './components/dialog-confirm/dialog-confirm.component';



@NgModule({
  declarations: [
    Question2Component,
    FormComponent,
    DialogConfirmComponent
  ],
  imports: [
    CommonModule,    
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule
  ],
  exports: [
    Question2Component
  ]
})
export class Question2Module { }
