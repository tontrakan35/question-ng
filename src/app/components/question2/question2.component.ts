import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import * as moment from 'moment';
import { filter, take, tap } from 'rxjs';
import Swal from 'sweetalert2';
import { DialogConfirmComponent } from './components/dialog-confirm/dialog-confirm.component';
import { FormComponent } from './components/form/form.component';

@Component({
  selector: 'app-question2',
  templateUrl: './question2.component.html',
  styleUrls: ['./question2.component.scss']
})
export class Question2Component implements OnInit {

  form!: FormGroup;
  data!: any;
  staffs!: any[];
  positions!: { [index: string]: string };
  date: string = moment().format('YYYYMMDDhhmmss');

  constructor(private fb: FormBuilder, public dialog: MatDialog){
    localStorage.setItem('positions', JSON.stringify([
      {code: "prog", name:"โปรเเกรมเมอร์"},
      {code: "design", name:"ดีไซเนอร์"},
      {code: "manager", name:"ผู้จัดการโครงการ"},
    ]));
   }

  ngOnInit(): void {
    this.loadstaffInfo();
    this.loadPositionInfo();
    this.form = this.fb.group({
      id: [''],
      fullName: ['', Validators.required],
      tel: ['', [Validators.required, Validators.pattern(/\d{3}-\d{3}-\d{4}/)]],
      position: ['', Validators.required]
    })
  }

  clrarForm() {
    this.form.patchValue({
      id: '',
      fullName: '',
      tel: '',
      position: '',
    })
  }

  openForm() {
    const dialog = this.dialog.open(FormComponent, {
      width: '90%',
      data: { form: this.form },
      disableClose: true
    })

    dialog.afterClosed().pipe(
      take(1),
      tap((item) => {
        
        if(!item) return;

        if(!item.id) {
          this.staffs.push({...item, id: this.generateId()});
        } else {
          this.staffs = this.staffs.map((data: any) => {
            if(data.id === item.id) {
              return {...data, ...item};
            }
            return {...data}
          })
        }
        localStorage.setItem('staffs', JSON.stringify(this.staffs));
        this.successpopup()
      }),
      tap(() => this.clrarForm())
    ).subscribe()
  }

  update(data: any) {
    this.form.patchValue({
      id: data.id,
      fullName: data.fullName,
      tel: data.tel,
      position: data.position,
    })
    this.openForm()
  }

  remove(id: string) {
    const dialog = this.dialog.open(DialogConfirmComponent, {
      width: 'auto',
      disableClose: true
    })

    dialog.afterClosed().pipe(
      take(1),
      filter((isSuccess) => !!isSuccess),
      tap(() => {
        this.staffs = this.staffs.filter((data: any) => data.id !== id)
        localStorage.setItem('staffs', JSON.stringify(this.staffs))
      }),
      tap(() => this.successpopup())
    ).subscribe()
  }

  loadstaffInfo() {
    const store = localStorage.getItem('staffs');
    const staffs = store ? JSON.parse(store) : [];
    this.staffs = [...staffs];
  }

  loadPositionInfo() {
    const store = localStorage.getItem('positions');
    const positions = store ? JSON.parse(store) : [];
    this.positions = positions.reduce((info: any, data: any) => {
      return info = {...info, [data.code]: data.name}
    }, {})
  }

  generateId() {
    const staffs = this.staffs
        ?.filter((item: {id: string}) => item.id.substring(0, 14) === this.date)
        ?.sort((a: any, b: any) => {
          return a > b ? -1 : (a == b ? 0 : 1)
        })

    let id: string = this.date;

    if(staffs?.length) {
      const no = (parseInt(staffs[0]?.id.substring(15)) + 1)?.toString();
      return `${id.padStart(2-no.length)}${no}`
    }
    return `${id}01`;
  }

  successpopup(text?:string) {
    Swal.fire({
      icon: 'success',
      title: text,
      showConfirmButton: false,
      timer: 1500
    })
  }

}
