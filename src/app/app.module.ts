import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule,  } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { Question1Component } from './components/question1/question1.component';
import { Question2Module } from './components/question2/question2.module';

@NgModule({
  declarations: [
    AppComponent,
    Question1Component,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    Question2Module
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
